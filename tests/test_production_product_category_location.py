# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
import unittest
import doctest
import trytond.tests.test_tryton
from trytond.tests.test_tryton import ModuleTestCase, with_transaction
from trytond.tests.test_tryton import doctest_teardown, doctest_checker
from trytond.exceptions import UserError
from trytond.pool import Pool


class ProductionProductCategoryLocationTestCase (ModuleTestCase):
    """Test Production Product Category Location module"""
    module = 'production_product_category_location'

    @with_transaction()
    def test_add_category_default_location(self):
        """Add the category default location for a warehouse"""
        pool = Pool()
        Location = pool.get('stock.location')
        Category = pool.get('product.category')
        CategoryLocation = pool.get('stock.product.category.location')

        warehouse, = Location.search([('code', '=', 'WH')])
        production, = Location.search([('code', '=', 'PROD')])
        Location.write([warehouse], {'production_location': production.id})
        new_prod, = Location.create([
            {
                'code': 'PROD_TEST',
                'name': 'Production location test',
                'type': 'production',
                'parent': production.id
            }
        ])
        new_cat, = Category.create([{'name': 'CAT1'}])
        cat_loc_rec = {
            'category': new_cat.id,
            'warehouse': warehouse.id,
            'location': new_prod.id
        }
        CategoryLocation.create([cat_loc_rec])

    @with_transaction()
    def test_category_location_unique(self):
        """Check unique error"""
        pool = Pool()
        Location = pool.get('stock.location')
        Category = pool.get('product.category')
        CategoryLocation = pool.get('stock.product.category.location')

        warehouse, = Location.search([('code', '=', 'WH')])
        production, = Location.search([('code', '=', 'PROD')])
        new_prod, = Location.create([
            {
                'code': 'PROD_TEST',
                'name': 'Production location test',
                'type': 'production',
                'parent': production.id
            }
        ])
        new_cat, = Category.create([{'name': 'CAT1'}])
        cat_loc_rec = {
            'category': new_cat.id,
            'warehouse': warehouse.id,
            'location': new_prod.id
        }
        CategoryLocation.create([cat_loc_rec])

        with self.assertRaises(UserError) as cm:
            CategoryLocation.create([cat_loc_rec])
        self.assertEqual(cm.exception.message,
            'Category and location must be unique.')


def suite():
    suite = trytond.tests.test_tryton.suite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(
        ProductionProductCategoryLocationTestCase))
    suite.addTests(doctest.DocFileSuite(
        'scenario_production_product_category_location.rst',
        tearDown=doctest_teardown, encoding='utf-8',
        checker=doctest_checker,
        optionflags=doctest.REPORT_ONLY_FIRST_FAILURE))
    return suite
